10_000_000.
number of methods: 3
number of query sets: 3
number of queries in each true or false: 0
DGraph fileName=./experiments/graphs//V5kD5L8exp.edge
dataset: ./experiments/graphs//V5kD5L8exp.edge with |L|=8 and |V|=5000
method i=0
runTestsPerIndex index=BFSBFS has index size (byte): 399760
BFS required index construction time (s): 0.00656925
Method: BFS queryset: 0
indexName=BFS,j=0,trueSum=0,falseSum=0
Method: BFS queryset: 1
indexName=BFS,j=1,trueSum=0,falseSum=0
Method: BFS queryset: 2
indexName=BFS,j=2,trueSum=0,falseSum=0
------
method i=1
buildIndex name=LI+OTH+EXTv2-b=20-k=1320,continueCode=0
LandmarkedIndex ordering.size()=5000,N=5000
LandmarkedIndex::determineLandmarks noOfLandmarks=1320 ,method=1
LI+OTH+EXTv2-b=20-k=1320-Index initialized
LI+OTH+EXTv2-b=20-k=1320::buildIndex build landmarks, quotum=66
LI+OTH+EXTv2-b=20-k=1320::buildIndex (landmarks) progress=65 4.92424% , ,roundNo=237568,Esize(kB)=11480,time(s)=2.23354
LI+OTH+EXTv2-b=20-k=1320::buildIndex (landmarks) progress=131 9.92424% , ,roundNo=268800,Esize(kB)=22880,time(s)=3.55558
LI+OTH+EXTv2-b=20-k=1320::buildIndex (landmarks) progress=197 14.9242% , ,roundNo=300032,Esize(kB)=33777,time(s)=4.56879
LI+OTH+EXTv2-b=20-k=1320::buildIndex (landmarks) progress=263 19.9242% , ,roundNo=331264,Esize(kB)=44916,time(s)=5.64444
LI+OTH+EXTv2-b=20-k=1320::buildIndex (landmarks) progress=329 24.9242% , ,roundNo=362496,Esize(kB)=56015,time(s)=6.65909
LI+OTH+EXTv2-b=20-k=1320::buildIndex (landmarks) progress=395 29.9242% , ,roundNo=393728,Esize(kB)=66809,time(s)=7.72975
LI+OTH+EXTv2-b=20-k=1320::buildIndex (landmarks) progress=461 34.9242% , ,roundNo=424960,Esize(kB)=77747,time(s)=8.62297
LI+OTH+EXTv2-b=20-k=1320::buildIndex (landmarks) progress=527 39.9242% , ,roundNo=456192,Esize(kB)=88574,time(s)=9.40034
LI+OTH+EXTv2-b=20-k=1320::buildIndex (landmarks) progress=593 44.9242% , ,roundNo=487424,Esize(kB)=99264,time(s)=10.0557
LI+OTH+EXTv2-b=20-k=1320::buildIndex (landmarks) progress=659 49.9242% , ,roundNo=518656,Esize(kB)=109769,time(s)=10.6448
LI+OTH+EXTv2-b=20-k=1320::buildIndex (landmarks) progress=725 54.9242% , ,roundNo=550144,Esize(kB)=120044,time(s)=11.1688
LI+OTH+EXTv2-b=20-k=1320::buildIndex (landmarks) progress=791 59.9242% , ,roundNo=581632,Esize(kB)=130735,time(s)=11.7076
LI+OTH+EXTv2-b=20-k=1320::buildIndex (landmarks) progress=857 64.9242% , ,roundNo=613120,Esize(kB)=140935,time(s)=12.1898
LI+OTH+EXTv2-b=20-k=1320::buildIndex (landmarks) progress=923 69.9242% , ,roundNo=644608,Esize(kB)=151284,time(s)=12.6757
LI+OTH+EXTv2-b=20-k=1320::buildIndex (landmarks) progress=989 74.9242% , ,roundNo=676096,Esize(kB)=161598,time(s)=13.1425
LI+OTH+EXTv2-b=20-k=1320::buildIndex (landmarks) progress=1055 79.9242% , ,roundNo=707840,Esize(kB)=171587,time(s)=13.5469
LI+OTH+EXTv2-b=20-k=1320::buildIndex (landmarks) progress=1121 84.9242% , ,roundNo=739584,Esize(kB)=181946,time(s)=13.9826
LI+OTH+EXTv2-b=20-k=1320::buildIndex (landmarks) progress=1187 89.9242% , ,roundNo=771328,Esize(kB)=192089,time(s)=14.4285
LI+OTH+EXTv2-b=20-k=1320::buildIndex (landmarks) progress=1253 94.9242% , ,roundNo=803072,Esize(kB)=201695,time(s)=14.7993
LI+OTH+EXTv2-b=20-k=1320::buildIndex (landmarks) progress=1319 99.9242% , ,roundNo=834816,Esize(kB)=210634,time(s)=15.0907
LI+OTH+EXTv2-b=20-k=1320::buildIndex built all landmarks size(kB)=210634 ,time(s)=0
LI+OTH+EXTv2-b=20-k=1320::buildIndex build others, quotum=184
LI+OTH+EXTv2-b=20-k=1320::buildIndex (others) (o) progress=183 4.97283% , ,roundNo=2,Esize(kB)=210690,time(s)=0.00193191
LI+OTH+EXTv2-b=20-k=1320::buildIndex (others) (o) progress=367 9.97283% , ,roundNo=2,Esize(kB)=210739,time(s)=0.00365996
LI+OTH+EXTv2-b=20-k=1320::buildIndex (others) (o) progress=551 14.9728% , ,roundNo=2,Esize(kB)=210791,time(s)=0.00538087
LI+OTH+EXTv2-b=20-k=1320::buildIndex (others) (o) progress=735 19.9728% , ,roundNo=2,Esize(kB)=210848,time(s)=0.00730991
LI+OTH+EXTv2-b=20-k=1320::buildIndex (others) (o) progress=919 24.9728% , ,roundNo=2,Esize(kB)=210900,time(s)=0.00910902
LI+OTH+EXTv2-b=20-k=1320::buildIndex (others) (o) progress=1103 29.9728% , ,roundNo=2,Esize(kB)=210952,time(s)=0.0107639
LI+OTH+EXTv2-b=20-k=1320::buildIndex (others) (o) progress=1287 34.9728% , ,roundNo=2,Esize(kB)=211008,time(s)=0.012439
LI+OTH+EXTv2-b=20-k=1320::buildIndex (others) (o) progress=1471 39.9728% , ,roundNo=2,Esize(kB)=211064,time(s)=0.014154
LI+OTH+EXTv2-b=20-k=1320::buildIndex (others) (o) progress=1655 44.9728% , ,roundNo=2,Esize(kB)=211117,time(s)=0.0158279
LI+OTH+EXTv2-b=20-k=1320::buildIndex (others) (o) progress=1839 49.9728% , ,roundNo=2,Esize(kB)=211170,time(s)=0.0174799
LI+OTH+EXTv2-b=20-k=1320::buildIndex (others) (o) progress=2023 54.9728% , ,roundNo=2,Esize(kB)=211226,time(s)=0.0192108
LI+OTH+EXTv2-b=20-k=1320::buildIndex (others) (o) progress=2207 59.9728% , ,roundNo=2,Esize(kB)=211279,time(s)=0.0208519
LI+OTH+EXTv2-b=20-k=1320::buildIndex (others) (o) progress=2391 64.9728% , ,roundNo=2,Esize(kB)=211336,time(s)=0.0226099
LI+OTH+EXTv2-b=20-k=1320::buildIndex (others) (o) progress=2575 69.9728% , ,roundNo=2,Esize(kB)=211392,time(s)=0.0243189
LI+OTH+EXTv2-b=20-k=1320::buildIndex (others) (o) progress=2759 74.9728% , ,roundNo=2,Esize(kB)=211447,time(s)=0.026031
LI+OTH+EXTv2-b=20-k=1320::buildIndex (others) (o) progress=2943 79.9728% , ,roundNo=2,Esize(kB)=211506,time(s)=0.0277019
LI+OTH+EXTv2-b=20-k=1320::buildIndex (others) (o) progress=3127 84.9728% , ,roundNo=5,Esize(kB)=211564,time(s)=0.0292859
LI+OTH+EXTv2-b=20-k=1320::buildIndex (others) (o) progress=3311 89.9728% , ,roundNo=2,Esize(kB)=211620,time(s)=0.03091
LI+OTH+EXTv2-b=20-k=1320::buildIndex (others) (o) progress=3495 94.9728% , ,roundNo=2,Esize(kB)=211678,time(s)=0.03251
LI+OTH+EXTv2-b=20-k=1320::buildIndex (others) (o) progress=3679 99.9728% , ,roundNo=1,Esize(kB)=211718,time(s)=0.0334499
LandmarkedIndex construction time(s)=15.7617
LandmarkedIndex size=213298700,isBlockedMode=0, noOfLandmarks=1320 ,propagateCode=0
runTestsPerIndex index=LI+OTH+EXTv2-b=20-k=1320LI+OTH+EXTv2-b=20-k=1320 has index size (byte): 213298700
LI+OTH+EXTv2-b=20-k=1320 required index construction time (s): 15.7617
Method: LI+OTH+EXTv2-b=20-k=1320 queryset: 0
indexName=LI+OTH+EXTv2-b=20-k=1320,j=0,trueSum=0,falseSum=0
Method: LI+OTH+EXTv2-b=20-k=1320 queryset: 1
indexName=LI+OTH+EXTv2-b=20-k=1320,j=1,trueSum=0,falseSum=0
Method: LI+OTH+EXTv2-b=20-k=1320 queryset: 2
indexName=LI+OTH+EXTv2-b=20-k=1320,j=2,trueSum=0,falseSum=0
------
method i=2
buildIndex name=LI-k=1320,continueCode=0
LandmarkedIndex ordering.size()=5000,N=5000
LandmarkedIndex::determineLandmarks noOfLandmarks=1320 ,method=1
LI-k=1320-Index initialized
LI-k=1320::buildIndex build landmarks, quotum=66
LI-k=1320::buildIndex (landmarks) progress=65 4.92424% , ,roundNo=1747968,Esize(kB)=1192,time(s)=2.0092
LI-k=1320::buildIndex (landmarks) progress=131 9.92424% , ,roundNo=1775104,Esize(kB)=2536,time(s)=3.19826
LI-k=1320::buildIndex (landmarks) progress=197 14.9242% , ,roundNo=1802496,Esize(kB)=4313,time(s)=4.10255
LI-k=1320::buildIndex (landmarks) progress=263 19.9242% , ,roundNo=1829888,Esize(kB)=7375,time(s)=5.07501
LI-k=1320::buildIndex (landmarks) progress=329 24.9242% , ,roundNo=1857280,Esize(kB)=10083,time(s)=5.96506
LI-k=1320::buildIndex (landmarks) progress=395 29.9242% , ,roundNo=1884928,Esize(kB)=13248,time(s)=6.91077
LI-k=1320::buildIndex (landmarks) progress=461 34.9242% , ,roundNo=1912576,Esize(kB)=17294,time(s)=7.71324
LI-k=1320::buildIndex (landmarks) progress=527 39.9242% , ,roundNo=1940224,Esize(kB)=22127,time(s)=8.41072
LI-k=1320::buildIndex (landmarks) progress=593 44.9242% , ,roundNo=1967872,Esize(kB)=26910,time(s)=8.99856
LI-k=1320::buildIndex (landmarks) progress=659 49.9242% , ,roundNo=1995520,Esize(kB)=32781,time(s)=9.53961
LI-k=1320::buildIndex (landmarks) progress=725 54.9242% , ,roundNo=2023168,Esize(kB)=37055,time(s)=10.0525
LI-k=1320::buildIndex (landmarks) progress=791 59.9242% , ,roundNo=2050816,Esize(kB)=43331,time(s)=10.5907
LI-k=1320::buildIndex (landmarks) progress=857 64.9242% , ,roundNo=2078464,Esize(kB)=48580,time(s)=11.0617
LI-k=1320::buildIndex (landmarks) progress=923 69.9242% , ,roundNo=2106112,Esize(kB)=54454,time(s)=11.539
LI-k=1320::buildIndex (landmarks) progress=989 74.9242% , ,roundNo=2133504,Esize(kB)=60871,time(s)=11.9959
LI-k=1320::buildIndex (landmarks) progress=1055 79.9242% , ,roundNo=2161408,Esize(kB)=67296,time(s)=12.4046
LI-k=1320::buildIndex (landmarks) progress=1121 84.9242% , ,roundNo=2189312,Esize(kB)=74004,time(s)=12.8379
LI-k=1320::buildIndex (landmarks) progress=1187 89.9242% , ,roundNo=2217216,Esize(kB)=79163,time(s)=13.2772
LI-k=1320::buildIndex (landmarks) progress=1253 94.9242% , ,roundNo=2245120,Esize(kB)=85542,time(s)=13.6425
LI-k=1320::buildIndex (landmarks) progress=1319 99.9242% , ,roundNo=2273024,Esize(kB)=89477,time(s)=13.9305
LI-k=1320::buildIndex built all landmarks size(kB)=89607 ,time(s)=0
LI-k=1320::buildIndex build others, quotum=184
LandmarkedIndex construction time(s)=13.9375
LandmarkedIndex size=211050548,isBlockedMode=0, noOfLandmarks=1320 ,propagateCode=0
runTestsPerIndex index=LI-k=1320LI-k=1320 has index size (byte): 211050548
LI-k=1320 required index construction time (s): 13.9375
Method: LI-k=1320 queryset: 0
indexName=LI-k=1320,j=0,trueSum=0,falseSum=0
Method: LI-k=1320 queryset: 1
indexName=LI-k=1320,j=1,trueSum=0,falseSum=0
Method: LI-k=1320 queryset: 2
indexName=LI-k=1320,j=2,trueSum=0,falseSum=0
------
method i=3
buildIndex name=LI-k=5000,continueCode=0
LandmarkedIndex ordering.size()=5000,N=5000
LandmarkedIndex::determineLandmarks noOfLandmarks=5000 ,method=1
LI-k=5000-Index initialized
LI-k=5000::buildIndex build landmarks, quotum=250
LI-k=5000::buildIndex (landmarks) progress=249 4.98% , ,roundNo=2520576,Esize(kB)=7907,time(s)=4.33956
LI-k=5000::buildIndex (landmarks) progress=499 9.98% , ,roundNo=2547200,Esize(kB)=20913,time(s)=7.17629
LI-k=5000::buildIndex (landmarks) progress=749 14.98% , ,roundNo=2574336,Esize(kB)=35946,time(s)=9.67466
LI-k=5000::buildIndex (landmarks) progress=999 19.98% , ,roundNo=2601472,Esize(kB)=50446,time(s)=12.136
LI-k=5000::buildIndex (landmarks) progress=1249 24.98% , ,roundNo=2628608,Esize(kB)=66950,time(s)=14.0531
LI-k=5000::buildIndex (landmarks) progress=1499 29.98% , ,roundNo=2656000,Esize(kB)=85965,time(s)=15.6895
LI-k=5000::buildIndex (landmarks) progress=1749 34.98% , ,roundNo=2683392,Esize(kB)=107192,time(s)=17.2434
LI-k=5000::buildIndex (landmarks) progress=1999 39.98% , ,roundNo=2711040,Esize(kB)=129296,time(s)=18.7801
LI-k=5000::buildIndex (landmarks) progress=2249 44.98% , ,roundNo=2738688,Esize(kB)=150751,time(s)=20.0569
LI-k=5000::buildIndex (landmarks) progress=2499 49.98% , ,roundNo=2766336,Esize(kB)=176003,time(s)=21.3365
LI-k=5000::buildIndex (landmarks) progress=2749 54.98% , ,roundNo=2793984,Esize(kB)=200410,time(s)=22.4824
LI-k=5000::buildIndex (landmarks) progress=2999 59.98% , ,roundNo=2821632,Esize(kB)=223360,time(s)=23.6417
LI-k=5000::buildIndex (landmarks) progress=3249 64.98% , ,roundNo=2849280,Esize(kB)=249691,time(s)=24.7645
LI-k=5000::buildIndex (landmarks) progress=3499 69.98% , ,roundNo=2876928,Esize(kB)=278021,time(s)=25.8519
LI-k=5000::buildIndex (landmarks) progress=3749 74.98% , ,roundNo=2904576,Esize(kB)=306278,time(s)=27.0034
LI-k=5000::buildIndex (landmarks) progress=3999 79.98% , ,roundNo=2932224,Esize(kB)=335621,time(s)=28.0589
LI-k=5000::buildIndex (landmarks) progress=4249 84.98% , ,roundNo=2959872,Esize(kB)=366294,time(s)=29.0439
LI-k=5000::buildIndex (landmarks) progress=4499 89.98% , ,roundNo=2987520,Esize(kB)=396493,time(s)=29.9216
LI-k=5000::buildIndex (landmarks) progress=4749 94.98% , ,roundNo=3015168,Esize(kB)=427242,time(s)=30.7176
LI-k=5000::buildIndex (landmarks) progress=4999 99.98% , ,roundNo=3042816,Esize(kB)=452448,time(s)=31.1836
LI-k=5000::buildIndex built all landmarks size(kB)=452546 ,time(s)=0
LI-k=5000::buildIndex build others, quotum=1
LandmarkedIndex construction time(s)=31.1906
LandmarkedIndex size=693758948,isBlockedMode=0, noOfLandmarks=5000 ,propagateCode=0
runTestsPerIndex index=LI-k=5000LI-k=5000 has index size (byte): 693758948
LI-k=5000 required index construction time (s): 31.1906
Method: LI-k=5000 queryset: 0
indexName=LI-k=5000,j=0,trueSum=0,falseSum=0
Method: LI-k=5000 queryset: 1
indexName=LI-k=5000,j=1,trueSum=0,falseSum=0
Method: LI-k=5000 queryset: 2
indexName=LI-k=5000,j=2,trueSum=0,falseSum=0
------
--- Writing out the data ---
method name & index construction time (s) & index size (MB) \\ 
BFS & <0.010000000000000 & 0.39\\ 
LI+OTH+EXTv2-b=20-k=1320 & 15.76 & 213.29\\ 
LI-k=1320 & 13.93 & 211.05\\ 
LI-k=5000 & 31.19 & 693.75\\ 
mean-true & nan & nan & nan & nan & nan & nan & nan & nan & nan & nan & nan & nan
mean-false & nan & nan & nan & nan & nan & nan & nan & nan & nan & nan & nan & nan
sd-true & nan & nan & nan & nan & nan & nan & nan & nan & nan & nan & nan & nan
sd-false & nan & nan & nan & nan & nan & nan & nan & nan & nan & nan & nan & nan
method name & queryset (true) 0 & queryset (false) 0 & queryset (true) 1 & queryset (false) 1 & queryset (true) 2 & queryset (false) 2\\ 
LI+OTH+EXTv2-b=20-k=1320 & na & na & na & na & na & na\\ 
LI-k=1320 & na & na & na & na & na & na\\ 
LI-k=5000 & na & na & na & na & na & na\\ 
