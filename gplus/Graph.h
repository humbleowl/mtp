#include "fsm.h"

void get_time_clock(struct timespec * ts){
#ifdef __MACH__

clock_serv_t cclock;
mach_timespec_t mts;
host_get_clock_service(mach_host_self(), CALENDAR_CLOCK, &cclock);
clock_get_time(cclock, &mts);
mach_port_deallocate(mach_task_self(), cclock);
ts->tv_sec = mts.tv_sec;
ts->tv_nsec = mts.tv_nsec;

#else

get_time_clock( CLOCK_MONOTONIC,ts);
#endif
}


class Graph{

public:
	Graph();
	Graph(char *graphfilename, char *featfilename,int numNodes);
	void readGraph(char *graphfilename);
	void readFeat(char *featfilename);
	void addEdge(int src, int dst);

	void readAndRunQuery(char *queryFileName, int control);
	int isReachable(int src, int dst, int control=1, int query_type=1, vector<int> query_labels=vector<int>());

	int numberOfNodes();
	int numberOfEdges();
	int diameter();

	/*For RandomReach*/
	int numWalks, numStops, walkLength;
	int *srcStops, *dstStops;
	int c_budget, c_walkLength, c_numWalks;
	void initializeRRParams(int diameter);
	int buildStops(int src, bool direction, int control, int query_type, vector<int> query_labels);
	bool rrParamsInitialized;
	double initializationTime;

	/*For measurement*/
	double queryTimeMean, queryTimeVar, insertTimeMean, insertTimeVar, deleteTimeMean, deleteTimeVar;
	double totalQueryTime, totalInsertTime, totalDeleteTime;
	int numInserts, numDeletes, numQueries;
	void sort_labels_all_nodes();
private:
	std::vector<Node*> nodes;
	int numNodes;
	int numEdges;

	void addEdgeToIndex(int src, int dst);
	void removeEdgeFromIndex(int src, int dst);

};


Graph::Graph(){
	numNodes = 0;
	numEdges = 0;

	rrParamsInitialized = false;
	initializationTime = 0;

	numQueries = 0;
	numDeletes = 0;
	numInserts = 0;
	queryTimeMean = 0;
	queryTimeVar = 0;
	totalQueryTime = 0;
	insertTimeMean = 0;
	insertTimeVar = 0;
	totalInsertTime = 0;
	deleteTimeMean = 0;
	deleteTimeVar = 0;
	totalDeleteTime = 0;
}

Graph::Graph(char *graphfilename, char *featfilename, int num = 0){


	cout<<"here"<<endl;
	//std::ifstream myfile(graphfilename);
	std::string line;
	initializationTime = 0;
	struct timespec start, finish;
	double elapsed;

	get_time_clock( &start);

	numNodes = num;
	nodes.push_back(new Node(0));
	for (int i = 0; i < numNodes; i++){
		nodes.push_back(new Node(i));
	}


	std::ifstream myfile3(featfilename);
	while (getline(myfile3,line)){
		if (line[0] == '#') continue;
		char *cstr = &line[0u];
		char *t = strtok(cstr," ");
		int u = atoi(t);
		t = strtok(NULL," ");
		int l = atoi(t);
		nodes[u]->add_label(l);
			if(label_map.find(l)==label_map.end())
			{
				vector<int> temp;
				temp.push_back(u);
				label_map[l] = temp;
			}
			else
				label_map[l].push_back(u);
        }
	myfile3.close();
	for (int i = 0; i < numNodes; i++){
		nodes[i]->sort_labels();
	}

	for(map<int,vector<int> >::iterator it = label_map.begin(); it != label_map.end(); ++it)
	{
		sort(label_map[it->first].begin(),label_map[it->first].end());
	} 
	
	std::ifstream myfile2(graphfilename);
	while (getline(myfile2,line)){
		if (line[0] == '#') continue;
		char *cstr = &line[0u];
		char *t = strtok(cstr," ");
		int u = atoi(t);
		t = strtok(NULL," ");
		int v = atoi(t);
		addEdge(u,v);
        }
	myfile2.close();
	
	
	get_time_clock( &finish);
	initializationTime += (finish.tv_sec - start.tv_sec) + (finish.tv_nsec - start.tv_nsec)/pow(10,9);

	rrParamsInitialized = false;

	numQueries = 0;
	numDeletes = 0;
	numInserts = 0;
	queryTimeMean = 0;
	queryTimeVar = 0;
	totalQueryTime = 0;
	insertTimeMean = 0;
	insertTimeVar = 0;
	totalInsertTime = 0;
	deleteTimeMean = 0;
	deleteTimeVar = 0;
	totalDeleteTime = 0;

}

void Graph::addEdge(int src, int dst){
	if ((src >= numNodes) || (dst >= numNodes)) return;
	nodes[src]->addEdge(dst, true, nodes[dst]->labels);
	nodes[dst]->addEdge(src, false, nodes[src]->labels);
	numEdges += 1;
	numInserts += 1;
}

void Graph::sort_labels_all_nodes(){
	for(int i=0;i<numNodes;i++)
		nodes[i]->sort_labels();
}

void Graph::readGraph(char *graphfilename){
	std::ifstream myfile(graphfilename);
	std::string line;
	struct timespec start, finish;
	get_time_clock( &start);
	while (getline(myfile,line)){
		if (line[0] == '#') continue;
		char *cstr = &line[0u];
		char *t = strtok(cstr," ");
		int u = atoi(t);
		t = strtok(NULL," ");
		int v = atoi(t);
		if ((u < numNodes) && (v < numNodes)){
			addEdge(u,v);
                }
        }
	myfile.close();
	get_time_clock( &finish);
	initializationTime += (finish.tv_sec - start.tv_sec) + (finish.tv_nsec - start.tv_nsec)/pow(10,9);

}

void Graph::readAndRunQuery(char *queryFileName, int control){
	std::ifstream myfile(queryFileName);
	std::string line;

		struct timespec start, finish;

	int dia = 6;
	dia = diameter();
	for (int i = 0; i < 10; i++){
		int d = diameter();
		if (d > dia) dia = d;
	}

	
	get_time_clock( &start);

	initializeRRParams(dia);

	get_time_clock( &finish);

	
	initializationTime += (finish.tv_sec - start.tv_sec) + (finish.tv_nsec - start.tv_nsec)/pow(10,9);   

	cout << "numWalks = " << numWalks << ", dia = " << dia << endl;
	int total_queries = 0;
	int tp=0;
	int fp=0;
	int tn=0;
	int fn=0;

	double mean_RR_time=0, mean_BFS_time=0,elapsed=0;

	vector<double> RR_times, BFS_times;

	while (getline(myfile, line)){
		total_queries += 1;
		char *cstr = &line[0u];
		char *t = strtok(cstr," ");

		int u = atoi(t);
		t = strtok(NULL," ");
		int v = atoi(t);
		t = strtok(NULL, " ");
		int query_type = atoi(t);
		vector<int> query_labels;
		t = strtok(NULL, " ");
		int num_labels = atoi(t);
		while(num_labels--)
		{
			t = strtok(NULL, " ");
			query_labels.push_back(atoi(t));
		}
		
		if(control==5)
		{	
			get_time_clock( &start);
			int x1 = isReachable(u,v,1,query_type,query_labels);
			get_time_clock( &finish);
			elapsed = (finish.tv_sec - start.tv_sec) + (finish.tv_nsec - start.tv_nsec)/pow(10,9);
			mean_BFS_time += elapsed;
			BFS_times.push_back(elapsed);
			cout<<"BFS:"<<elapsed<<" ";

			get_time_clock( &start);
			int x2 = isReachable(u,v,2,query_type,query_labels);
			get_time_clock( &finish);
			elapsed = (finish.tv_sec - start.tv_sec) + (finish.tv_nsec - start.tv_nsec)/pow(10,9);
			mean_RR_time += elapsed;
			RR_times.push_back(elapsed);
			cout<<"RR:"<<elapsed<<" ";

			if(x2==0 && x1==1)
				fn+=1;
			if(x2==0 && x1==0)
				tn+=1;
			else if(x2==1 && x1==1)
				tp +=1;
			else
				fp +=1;

			std::cout << u << " " << v << " " << query_type<<" "<< x1 << " " << x2 << std::endl << std::flush;
		}
		else if(control==6)
		{		
				if(query_type==3)
				{
					int last_label = query_labels[query_labels.size()-1];
					if(!binary_search(label_map[last_label].begin(),label_map[last_label].end(),v))
						continue;
				}
				else if(query_type==2)
				{
					int flag = 0;
					for(int i=0;i<query_labels.size();i++)
					{
						int last_label = query_labels[i];
						if(binary_search(label_map[last_label].begin(),label_map[last_label].end(),v))
							flag = 1;
					}
					if(!flag)
						continue;
				}

				get_time_clock( &start);
				int x1 = isReachable(u,v,3,query_type,query_labels);
				get_time_clock( &finish);
				elapsed = (finish.tv_sec - start.tv_sec) + (finish.tv_nsec - start.tv_nsec)/pow(10,9);
				mean_BFS_time += elapsed;
				BFS_times.push_back(elapsed);
				cout<<"BFS:"<<elapsed<<" ";

				get_time_clock( &start);
				int x2 = isReachable(u,v,4,query_type,query_labels);
				get_time_clock( &finish);
				elapsed = (finish.tv_sec - start.tv_sec) + (finish.tv_nsec - start.tv_nsec)/pow(10,9);
				mean_RR_time += elapsed;
				RR_times.push_back(elapsed);
				cout<<"RR:"<<elapsed<<" ";

				if(x2==0 && x1==1)
					fn+=1;
				if(x2==0 && x1==0)
					tn+=1;
				else if(x2==1 && x1==1)
					tp +=1;
				else
					fp +=1;

				std::cout << u << " " << v << " " << query_type<<" "<< x1 << " " << x2 << std::endl << std::flush;
		}
		else
		{
			get_time_clock( &start);
			int x1 = isReachable(u,v,control,query_type,query_labels);
			get_time_clock( &finish);
			elapsed = (finish.tv_sec - start.tv_sec) + (finish.tv_nsec - start.tv_nsec)/pow(10,9);
			mean_RR_time += elapsed;
			RR_times.push_back(elapsed);

			std::cout << "Q: " << u << " " << v << " " << x1 << " " << elapsed << std::endl << std::flush;
		}

		numQueries += 1;
		queryTimeMean += mean_RR_time;
		queryTimeVar += mean_RR_time * mean_RR_time;		
	}
	myfile.close();
	queryTimeMean /= numQueries;
	queryTimeVar /= numQueries;
	queryTimeVar = sqrt(queryTimeVar - queryTimeMean*queryTimeMean);
	mean_BFS_time /= numQueries;
	mean_RR_time /= numQueries;

	cout<<"Recall: "<<(float)tp/(tp+fn)<<endl;
	cout<<"Accuracy: "<<(float)(tp+tn)/(total_queries)<<endl;
	cout<<"Mean BFS Time: "<<mean_BFS_time<<endl;
	cout<<"Mean RR Time: "<<mean_RR_time<<endl;

	cout<<"\nBFS Time Vector"<<endl;
	for(int i=0;i<BFS_times.size();i++)
		cout<<BFS_times[i]<<" ";

	cout<<"\nRR Time Vector"<<endl;
	for(int i=0;i<RR_times.size();i++)
		cout<<RR_times[i]<<" ";
	cout<<endl;
}

int Graph::numberOfNodes(){
	return numNodes;
}

int Graph::numberOfEdges(){
	return numEdges;
}

void Graph::initializeRRParams(int diameter){
	walkLength = (int) floor(c_walkLength * diameter);
	numStops = (int)(floor(c_numWalks*sqrt(numNodes * log(numNodes))));
	numWalks = numStops/walkLength;
	if (numWalks == 0) numWalks = 1;

	srcStops = (int*)malloc(sizeof(int) * (numStops + 1));
	dstStops = (int*)malloc(sizeof(int) * (numStops + 1));

	rrParamsInitialized = true;
}

int Graph::diameter(){
	vector<int> color;
	for (int i = 0; i < numNodes; i++) color.push_back(0);
	int src = rand() % numNodes;
	color[src] = 1;
	vector<int> queue;
	int u = src;
	while (u >= 0){
		Node *n = nodes[u];
		int numE = n->getNumEdges(true);
		for (int i = 0; i < numE; i++){
			int v = n->getEdgeAt(i, true);
			if (color[v] > 0) continue;
			color[v] = color[u] + 1;
			queue.push_back(v);
		}
		if (queue.size() == 0) break;
		u = queue[0];
		queue.erase(queue.begin());
	}
	int dia = 1;
	for (int i = 0; i < numNodes; i++)
		if (dia < (color[i]-1))
			dia = color[i]-1;
	return dia;
}

int Graph::buildStops(int src, bool direction, int control, int query_type, vector<int> query_labels ) {

	int *stops = srcStops;
	if (!direction) stops = dstStops;
	

	int stopIndex = 0;
	stops[stopIndex] = src;

	for (int i = 0; i < numWalks; i++){

		stack<int> cur_stack;
		int currNode = src;
		cur_stack.push(currNode);
		vector<int> visited_nodes;
		visited_nodes.push_back(currNode);

		int e = src;
		
		int l0 = 0;//used for FSM in query types 3

		for (int j = 0; j < walkLength; j++){
			Node *n = nodes[e];		
			if(control==2)	
				e = n->sampleEdge(direction);
			else if(control==4)
				{
					e = n->sampleEdge(direction);
					bool flag = false;
					for(int k=0;k<10 && e!=-1;k++)
					{	
						
						if(query_type==1)
							{	
								int label_id = query_labels[0];
								if(binary_search(label_map[label_id].begin(),label_map[label_id].end(),e))
								{
									flag = true;
									break;
								}
							}
						else if(query_type==2)
							{
								int l1,l2;
								l1 = 0;
								l2 = 0;
								bool flag1 = 0;
								while(l1<(nodes[e]->labels).size() && l2< query_labels.size() )
								{
									if((nodes[e]->labels)[l1] < query_labels[l2])
										l1++;
									else if((nodes[e]->labels)[l1] > query_labels[l2])
										l2++;
									else
									{
										flag1 = true;
										break;
									}
								}
								if(flag1)
								{
									flag = true;
									break;
								}
							}
						else if(query_type == 3)
							{	
								int label_id = query_labels[(l0+1)%query_labels.size()];
								if(binary_search(label_map[label_id].begin(),label_map[label_id].end(),e))
									{
										flag = true;
										l0 = (l0+1)%query_labels.size();
										break;
										
									}
							}	
						e = n->sampleEdge(direction);
					}
					if(!flag)
						{
							e = src;
							l0 = 0;
						}

				}

			if (e == -1){
				e = src;
			}

		stopIndex += 1;
		if (stopIndex <= numStops){
			stops[stopIndex] = e;				
			}
		}
		

		
	}
	return stopIndex;
}


int Graph::isReachable(int src, int dst, int control, int query_type, vector<int> query_labels){ //c ontrol=1 for BFS, 0 for RW
	if(src==dst) return 1;

	if(control==1){
		bool isReached[numNodes];
		std::vector<int> queue;
		for (int i = 0; i < numNodes; i++) isReached[i] = false;
		int u = src;
		isReached[u] = true;
		queue.push_back(u);
		while (queue.size() > 0){
			u = queue[0];
			queue.erase(queue.begin());
			//for each edge from u
			Node *n = nodes[u];

			for (int i = 0; i < n->getNumEdges(true); i++){
				int v = n->getEdgeAt(i,true);
				if (isReached[v] == false){
					isReached[v] = true;
					if(v==dst)
						return 1;
					queue.push_back(v);
				}
			}
		}

	}


	else if(control==2 || control==4 )
	{	

		int numSrcStops = buildStops(src, true, control, query_type, query_labels);
		if(query_type==3)
			{
				reverse(query_labels.begin(),query_labels.end());
			}
		int numDstStops = buildStops(dst, false, control, query_type, query_labels);
		int answer = 0;
		
		sort(srcStops,srcStops + numSrcStops);
		sort(dstStops,dstStops + numDstStops);
		int i =0;
		int j=0;
		while(i<numSrcStops && j<numDstStops)
			if(srcStops[i] < dstStops[j])
				i++;
			else if(srcStops[i] > dstStops[j])
				j++;
			else
				return 1;
		return 0;

	}
	else if(control==3)
	{	
		map<pair<int,int>, bool> isReached;
		std::vector<pair<int,int> > queue;
		int u = src;
		isReached[make_pair(u,query_labels[0])] = true;
		queue.push_back(make_pair(u,0));
		while (queue.size() > 0){
			u = queue[0].first;
			int l0 = queue[0].second;
			queue.erase(queue.begin());
			Node *n = nodes[u];
			vector<int> edges = n->return_edges(true);
			vector<pair<int,int> > cands;
			if(query_type==1)
			{	
				int label_id = query_labels[0];
				for(int i=0;i<edges.size();i++)
					if(binary_search(label_map[label_id].begin(),label_map[label_id].end(),edges[i]))
						cands.push_back(make_pair(edges[i],label_id));
			}
			else if(query_type==2)
			{
				
				for(int i=0;i<edges.size();i++)
				{	
					int e = edges[i];
					int l1,l2;
					l1 = 0;
					l2 = 0;
					while(l1<(nodes[e]->labels).size() && l2< query_labels.size() )
					{
						if((nodes[e]->labels)[l1] < query_labels[l2])
							l1++;
						else if((nodes[e]->labels)[l1] > query_labels[l2])
							l2++;
						else
						{
							cands.push_back(make_pair(edges[i],query_labels[l2]));
							break;

						}
					}
				}
			}
			else if(query_type==3)
			{	
				int label_id = query_labels[(l0+1)%query_labels.size()];
				for(int i=0;i<edges.size();i++)
					if(binary_search(label_map[label_id].begin(),label_map[label_id].end(),edges[i]))
						cands.push_back(make_pair(edges[i],(l0+1)%query_labels.size()));
			}
			for (int i = 0; i < cands.size(); i++){
				int v = cands[i].first;
				int l = cands[i].second;
				if (isReached.find(make_pair(v,l))==isReached.end()) {
					isReached[make_pair(v,l)] = true;
					if(v==dst)
						return 1;
					queue.push_back(make_pair(v,l));
				}
			}
		}
	}

	return 0;
}


