import glob

assn_filenames = glob.glob('*.feat');
feat_filenames = glob.glob('*.featnames');

nodes = []
label_names = {}
all_features = []

f = open("gplus_combined.txt")

mapping = {}
nodes = []
for line in f.readlines():
	ws = line.split()
	w1 = ws[0].strip()
	w2 = ws[1].strip()
	nodes.append(w1)
	nodes.append(w2)

nodes = list(set(nodes))

f.close()

print len(nodes)

for i in range(len(nodes)):
	mapping[nodes[i]] = i

# f1 = open('gplus_combined.txt')
# f2 = open('gplus_combined_std.txt','w')
# for line in f1.readlines():
# 	ws = line.split()
# 	w1 = ws[0].strip()
# 	w2 = ws[1].strip()
# 	f2.write(str(mapping[w1]) + " " + str(mapping[w2]) + "\n")


# f1.close()
# f2.close()

for ind in range(len(feat_filenames)):
	assn_file = open(assn_filenames[ind],'r')
	feat_file = open(feat_filenames[ind],'r')

	feature_names = [""] * 10000

	for line in feat_file.readlines():
		i = line.split()[0]
		feature_names[int(i)] = line[len(i)+1:].strip()
		all_features.append(line[len(i)+1:].strip())

	for line in assn_file.readlines():
		ws = line.split()
		node = mapping[ws[0]]
		label_names[node] = []
		nodes.append(mapping[ws[0]])
		for i in range(1,len(ws)):
			if(ws[i]=="1"):
				label_names[mapping[ws[0]]].append(feature_names[i-1])


all_features = list(set(all_features))

feat_to_id = {}

feat_ofile = open("gplus_combined_featnames.txt","w")
assn_ofile = open("gplus_combined_label_ids.txt","w")

nodes.sort()
print(len(nodes))

for i in range(len(all_features)):
	feat_to_id[all_features[i]] = i
	print >>feat_ofile, str(i)+" "+all_features[i]+"\n"

for node in nodes:
	try:
		for label in label_names[node]:
				assn_ofile.write(str(node) + " " + str(feat_to_id[label]) + "\n")
	except:
		dummy = 1

assn_ofile.close()

