

f = open("gplus_combined.txt")

mapping = {}
nodes = []
for line in f.readlines():
	ws = line.split()
	w1 = ws[0].strip()
	w2 = ws[1].strip()
	nodes.append(w1)
	nodes.append(w2)

nodes = list(set(nodes))

f.close()

print len(nodes)

for i in range(len(nodes)):
	mapping[nodes[i]] = i

f1 = open('gplus_combined.txt')
f2 = open('gplus_combined_std.txt','w')
for line in f1.readlines():
	ws = line.split()
	w1 = ws[0].strip()
	w2 = ws[1].strip()
	f2.write(str(mapping[w1]) + " " + str(mapping[w2]) + "\n")


f1.close()
f2.close()
