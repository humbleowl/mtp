#include <vector>
#include <queue>
#include <string>
#include <iostream>
#include <stdlib.h>
#include <cstdlib>
#include <ctime>
#include <fstream>
#include <string>
#include <algorithm>
#include <string.h>
#include <math.h>
#include <queue>
#include <stack>
#include <map>
#include <utility>

#include <time.h>
#include <sys/time.h>

#ifdef __MACH__
#include <mach/clock.h>
#include <mach/mach.h>
#endif

using namespace std;

map<int,vector<int> > label_map;

class Node{

public:
	Node(int isNodeId, std::string isNodeLabelling="");
	void addEdge(int destId, bool direction, vector<int> &label_ids);

	int getNumEdges(bool direction);
	bool has_label(int label_id);
	int getEdgeAt(int i, bool direction);
	int sample_new_Edge(bool direction, vector<int> visited);
	int sample_labelled_Edge(bool direction, int label);
	int sampleEdge(bool direction);
    void set_label(std::string s);
    void sort_labels();
    void add_label(int l);
    vector<int> return_edges(int direction);
    vector<int> return_labelled_edges(int l,int direction,int dst=0);
    int nodeId;
    std::vector<int> labels;

private:
	std::string labelling;
	std::vector<int> fwdEdges;
	std::vector<int> bwdEdges;
	int numFwdEdges;
	int numBwdEdges;
	map<int,vector<int> > fwd_labelled_edges;
	map<int,vector<int> > bwd_labelled_edges;
};

Node::Node(int isNodeId, std::string isNodeLabelling){
	nodeId = isNodeId;
	labelling = isNodeLabelling;
	numFwdEdges = 0;
	numBwdEdges = 0;
}

void Node::set_label(std::string s){
	labelling = s;
}
void Node::addEdge(int destId, bool direction, vector<int> &label_ids ){
	if (direction){
		fwdEdges.push_back(destId);
		numFwdEdges += 1;
		// for(int i=0;i<label_ids.size();i++)
		// 	if(fwd_labelled_edges.find(label_ids[i]) == fwd_labelled_edges.end() )
		// 	{
		// 		vector<int> temp;
		// 		temp.push_back(destId);
		// 		fwd_labelled_edges[label_ids[i]] = temp;
		// 	}
		// 	else
		// 		fwd_labelled_edges[label_ids[i]].push_back(destId);
	}
	else{
		bwdEdges.push_back(destId);
		numBwdEdges += 1;

		// for(int i=0;i<label_ids.size();i++)
		// 	if(bwd_labelled_edges.find(label_ids[i]) == bwd_labelled_edges.end() )
		// 	{
		// 		vector<int> temp;
		// 		temp.push_back(destId);
		// 		bwd_labelled_edges[label_ids[i]] = temp;
		// 	}
		// 	else
		// 		bwd_labelled_edges[label_ids[i]].push_back(destId);
	}
}
void Node::add_label(int l){
	labels.push_back(l);
}
void Node::sort_labels(){
	sort(labels.begin(),labels.end());
}

int Node::getNumEdges(bool direction){
	if (direction) return numFwdEdges;
	return numBwdEdges;
}

int Node::getEdgeAt(int i, bool direction){
	if (direction) return (i<numFwdEdges)?fwdEdges[i]:-1;
	return (i<numBwdEdges)?bwdEdges[i]:-1;
}

int Node::sample_new_Edge(bool direction, vector<int> visited){
	int n = getNumEdges(direction);
	vector<int> cand;

	if(direction){
		int k = 0;
		for(int i=0;i<numFwdEdges;i++)
		{	
			int flag = 0;
			for(int j=0;j<visited.size();j++)
				if(fwdEdges[i]==visited[j])
					flag = 1;
			if(!flag)
			{
			  cand.push_back(fwdEdges[i]);
			   k++;
			}
		}
	    if(k>0) return cand[rand()%k]; else return -1;
	}
	else{
		int k = 0;
		for(int i=0;i<numBwdEdges;i++)
		{	
			int flag = 0;
			for(int j=0;j<visited.size();j++)
				if(bwdEdges[i]==visited[j])
					flag = 1;
			if(!flag)
			{
			  cand.push_back(bwdEdges[i]);
			   k++;
			}
		}				
	    if(k>0) return cand[rand()%k]; else return -1;
	}
	return -1;
}

int Node::sample_labelled_Edge(bool direction, int l){
	// if(direction){
	// 	int k = 0;
	// 	if(fwd_labelled_edges.find(next_label) != fwd_labelled_edges.end())
	// 		k = fwd_labelled_edges[next_label].size();
	    
	//     if(k>0) return fwd_labelled_edges[next_label][rand()%k]; else return -1;	   
	// }
	// else{
	// 	int k = 0;
	// 	if(bwd_labelled_edges.find(next_label) != bwd_labelled_edges.end())
	// 		k = bwd_labelled_edges[next_label].size();
	    
	//     if(k>0) return bwd_labelled_edges[next_label][rand()%k]; else return -1;	   
	// }
	int n = getNumEdges(direction);
	vector<int> cand;

	if(direction){
		int k = 0;
		for(int i=0;i<numFwdEdges;i++)
		{	
			
			if(binary_search(label_map[l].begin(),label_map[l].end(),fwdEdges[i]))
			{
			  cand.push_back(fwdEdges[i]);
			   k++;
			}
		}
	    if(k>0) return cand[rand()%k]; else return -1;
	}
	else{
		int k = 0;
		for(int i=0;i<numBwdEdges;i++)
		{	
			
			if(binary_search(label_map[l].begin(),label_map[l].end(),bwdEdges[i]))
			{
			  cand.push_back(bwdEdges[i]);
			   k++;
			}
		}				
	    if(k>0) return cand[rand()%k]; else return -1;
	}
	return -1;
}

int Node::sampleEdge(bool direction){
	if(direction){
		int k = fwdEdges.size();
	    if(k>0) return fwdEdges[rand()%k]; else return -1;	   
	}
	else{
		int k = bwdEdges.size();	    
	    if(k>0) return bwdEdges[rand()%k]; else return -1;		   
	}
}

vector<int> Node::return_labelled_edges(int l,int direction,int dst){
	// if(direction)
	// 	if(fwd_labelled_edges.find(l) != fwd_labelled_edges.end())
	// 		return fwd_labelled_edges[l];
	// 	else
	// 		return (vector<int>());
	// else
	// 	if(bwd_labelled_edges.find(l) != bwd_labelled_edges.end())
	// 		return bwd_labelled_edges[l];
	// 	else
	// 		return (vector<int>());
	int n = getNumEdges(direction);
	vector<int> cand;

	if(direction){
		int k = 0;
		for(int i=0;i<numFwdEdges;i++)
		{	
			if( binary_search(label_map[l].begin(),label_map[l].end(),fwdEdges[i]) || fwdEdges[i] == dst )
			{
			  cand.push_back(fwdEdges[i]);
			}
		}
	}
	else{
		int k = 0;
		for(int i=0;i<numBwdEdges;i++)
		{	
			if( binary_search(label_map[l].begin(),label_map[l].end(),bwdEdges[i]) || bwdEdges[i] == dst )
			{
			  cand.push_back(bwdEdges[i]);
			}
		}				
	}

	return cand;

}

vector<int> Node::return_edges(int direction){

	if(direction){
		return fwdEdges;
	}
	else{
		return bwdEdges;		
	}


}

