
#include "Graph.h"


int main(int argc, char *argv[]){

	//cout<<"here"<<endl;

	if (argc < 4){
		cout << "Usage: <TemporalReachability> graphFile featFile queryFile control" << endl;
		return 0;
	}
	char *graphFile = argv[1];
	char *featFile = argv[2];
	char *queryFile = argv[3];
	int control = atoi(argv[4]);
	int c = 1, c_walkLength = 2, c_numWalks = 1, c_budget = 1;
	bool fractional = false;

	//cout<<"here"<<endl;

	srand(time(NULL));
	Graph *tg = new Graph(graphFile, featFile, 107615);

	tg->c_numWalks = atoi(argv[5]);
	tg->c_walkLength = atoi(argv[6]);

	cout << "1. Initialization Time = " << tg->initializationTime << ", Memory (MBs) = " << endl;

	cout << "Read Graph" << endl << flush;
	tg->readAndRunQuery(queryFile,control);

	cout << tg->insertTimeMean << " " << tg->insertTimeVar << " " <<
		tg->deleteTimeMean << " " << tg->deleteTimeVar << " " <<
		tg->queryTimeMean << " " << tg->queryTimeVar << endl;
	cout << "Initialization Time = " << tg->initializationTime << ", Memory (MBs) = " << endl;
}
