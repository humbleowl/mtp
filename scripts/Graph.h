#include "Node.h"


void get_time_clock(struct timespec * ts){
#ifdef __MACH__

clock_serv_t cclock;
mach_timespec_t mts;
host_get_clock_service(mach_host_self(), CALENDAR_CLOCK, &cclock);
clock_get_time(cclock, &mts);
mach_port_deallocate(mach_task_self(), cclock);
ts->tv_sec = mts.tv_sec;
ts->tv_nsec = mts.tv_nsec;

#else

get_time_clock( CLOCK_MONOTONIC,ts);
#endif
}


class Graph{

public:
	Graph();
	Graph(char *graphfilename, char *featfilename,int numNodes);
	void readGraph(char *graphfilename);
	void readFeat(char *featfilename);
	void addEdge(int src, int dst);

	void readAndRunQuery(char *queryfilename, int c = 1, bool fractional = false, int control= 1);
	int isReachable(int u, int v, int lablel_id, int c = 1, bool fractional = false, int control= 1);

	int numberOfNodes();
	int numberOfEdges();
	int diameter();

	/*For RandomReach*/
	int numWalks, numStops, walkLength;
	Node *srcStops, *dstStops;
	int c_budget, c_walkLength, c_numWalks;
	void initializeRRParams(int diameter);
	int buildStops(int sourceNode, bool direction, int label_id);
	bool rrParamsInitialized;
	double initializationTime;

	/*For measurement*/
	double queryTimeMean, queryTimeVar, insertTimeMean, insertTimeVar, deleteTimeMean, deleteTimeVar;
	double totalQueryTime, totalInsertTime, totalDeleteTime;
	int numInserts, numDeletes, numQueries;

private:
	std::vector<Node*> nodes;
	int numNodes;
	int numEdges;

	void addEdgeToIndex(int src, int dst);
	void removeEdgeFromIndex(int src, int dst);

};


Graph::Graph(){
	numNodes = 0;
	numEdges = 0;

	rrParamsInitialized = false;
	initializationTime = 0;

	numQueries = 0;
	numDeletes = 0;
	numInserts = 0;
	queryTimeMean = 0;
	queryTimeVar = 0;
	totalQueryTime = 0;
	insertTimeMean = 0;
	insertTimeVar = 0;
	totalInsertTime = 0;
	deleteTimeMean = 0;
	deleteTimeVar = 0;
	totalDeleteTime = 0;
}

Graph::Graph(char *graphfilename, char *featfilename, int num = 0){


	cout<<"here"<<endl;
	//std::ifstream myfile(graphfilename);
	std::string line;
	initializationTime = 0;
	struct timespec start, finish;
	double elapsed;

	get_time_clock( &start);

	// std::ifstream sfile1(featfilename);
	// int i = 0;
	// while (getline(sfile1,line)){
	// 	char *cstr = &line[0u];
	// 	if(i%2==0){
	// 		char *cstr = &line[0u];
	// 		char *t = strtok(cstr," ");
	// 		int u = atoi(t);
	// 		t = strtok(NULL," ");
	// 		if(u >= numNodes) numNodes = u + 1;
	// 	}	
	// 	i+=1;
	// }
	// sfile1.close();


	// numEdges = 0;
	// nodes.push_back(new Node(0));
	// for (int i = 0; i < numNodes; i++){
	// 	nodes.push_back(new Node(i));
	// }
	//cout<<"here"<<endl;

	// while (getline(myfile,line)){
	// 	if (line[0] == '#') continue;
	// 	char *cstr = &line[0u];
	// 	char *t = strtok(cstr," ");
	// 	int u = atoi(t);
	// 	t = strtok(NULL," ");
	// 	int v = atoi(t);
	// 	if (u >= numNodes) numNodes = u + 1;
	// 	if (v >= numNodes) numNodes = v + 1;
	// 	// addEdge(u,v);
 //        }
	// myfile.close();

	numNodes = num;
	nodes.push_back(new Node(0));
	for (int i = 0; i < numNodes; i++){
		nodes.push_back(new Node(i));
	}

	std::ifstream myfile2(graphfilename);
	while (getline(myfile2,line)){
		if (line[0] == '#') continue;
		char *cstr = &line[0u];
		char *t = strtok(cstr," ");
		int u = atoi(t);
		t = strtok(NULL," ");
		int v = atoi(t);
		addEdge(u,v);
        }
	myfile2.close();
	

	// std::ifstream sfile(featfilename);
	// i=0;
	// while (getline(sfile,line)){
	// 	char *cstr = &line[0u];
	// 	if(i%2==0){
	// 		char *cstr = &line[0u];
	// 		char *t = strtok(cstr," ");
	// 		int u = atoi(t);
	// 		t = strtok(NULL," ");
	// 		nodes[u]->set_label((std::string)t);
	// 	}
	// 	i+=1;
	// }
	// sfile.close();
	
	get_time_clock( &finish);
	initializationTime += (finish.tv_sec - start.tv_sec) + (finish.tv_nsec - start.tv_nsec)/pow(10,9);

	rrParamsInitialized = false;

	numQueries = 0;
	numDeletes = 0;
	numInserts = 0;
	queryTimeMean = 0;
	queryTimeVar = 0;
	totalQueryTime = 0;
	insertTimeMean = 0;
	insertTimeVar = 0;
	totalInsertTime = 0;
	deleteTimeMean = 0;
	deleteTimeVar = 0;
	totalDeleteTime = 0;

}

void Graph::addEdge(int src, int dst){
	if ((src >= numNodes) || (dst >= numNodes)) return;
	nodes[src]->addEdge(dst, true);
	nodes[dst]->addEdge(src, false);
	numEdges += 1;
	numInserts += 1;
}


void Graph::readGraph(char *graphfilename){
	std::ifstream myfile(graphfilename);
	std::string line;
	struct timespec start, finish;
	get_time_clock( &start);
	while (getline(myfile,line)){
		if (line[0] == '#') continue;
		char *cstr = &line[0u];
		char *t = strtok(cstr," ");
		int u = atoi(t);
		t = strtok(NULL," ");
		int v = atoi(t);
		if ((u < numNodes) && (v < numNodes)){
			addEdge(u,v);
                }
        }
	myfile.close();
	get_time_clock( &finish);
	initializationTime += (finish.tv_sec - start.tv_sec) + (finish.tv_nsec - start.tv_nsec)/pow(10,9);

}

void Graph::readAndRunQuery(char *queryFileName, int c, bool fractional, int control){
	std::ifstream myfile(queryFileName);
	std::string line;

		struct timespec start, finish;

	int dia = 6;
	dia = diameter();
	for (int i = 0; i < 10; i++){
		int d = diameter();
		if (d > dia) dia = d;
	}

	
	get_time_clock( &start);

	initializeRRParams(dia);

	get_time_clock( &finish);

	
	initializationTime += (finish.tv_sec - start.tv_sec) + (finish.tv_nsec - start.tv_nsec)/pow(10,9);   

	cout << "numWalks = " << numWalks << ", dia = " << dia << endl;
	int total_queries = 0;
	int tp=0;
	int fp=0;
	int tn=0;
	int fn=0;
	while (getline(myfile, line)){
		total_queries += 1;
		char *cstr = &line[0u];
		char *t = strtok(cstr," ");

		int u = atoi(t);
		t = strtok(NULL," ");
		int v = atoi(t);
		t = strtok(NULL, " ");
		int label_id = atoi(t);


		get_time_clock( &start);
		if(control==2)
		{
			int x1 = isReachable(u,v,label_id, c, fractional, 0);
			int x2 = isReachable(u,v,label_id, c, fractional, 1);

			if(x2==0 && x1==1)
				fp+=1;
			if(x2==0 && x1==0)
				tn+=1;
			else if(x2==1 && x1==1)
				tp +=1;
			else
				fn +=1;


		}
		int answer = isReachable(u,v,label_id, c, fractional, control);

		get_time_clock( &finish);
		double elapsed = (finish.tv_sec - start.tv_sec) + (finish.tv_nsec - start.tv_nsec)/pow(10,9);
		numQueries += 1;
		queryTimeMean += elapsed;
		queryTimeVar += elapsed * elapsed;
		std::cout << "Q: " << u << " " << v << " " << answer << " " << elapsed << std::endl << std::flush;
	}
	myfile.close();
	queryTimeMean /= numQueries;
	queryTimeVar /= numQueries;
	queryTimeVar = sqrt(queryTimeVar - queryTimeMean*queryTimeMean);

	cout<<"Recall: "<<(float)tp/(tp+fn)<<endl;
	cout<<"Accuracy: "<<(float)(tp+tn)/(total_queries)<<endl;
}

int Graph::numberOfNodes(){
	return numNodes;
}

int Graph::numberOfEdges(){
	return numEdges;
}

void Graph::initializeRRParams(int diameter){
	walkLength = (int) floor(c_walkLength * diameter);
	numStops = (int)(floor(c_numWalks*sqrt(numNodes * log(numNodes))));
	numWalks = numStops/walkLength;
	if (numWalks == 0) numWalks = 1;

	srcStops = (Node*)malloc(sizeof(Node) * (numStops + 1));
	dstStops = (Node*)malloc(sizeof(Node) * (numStops + 1));

	rrParamsInitialized = true;
}

int Graph::diameter(){
	vector<int> color;
	for (int i = 0; i < numNodes; i++) color.push_back(0);
	int src = rand() % numNodes;
	color[src] = 1;
	vector<int> queue;
	int u = src;
	while (u >= 0){
		Node *n = nodes[u];
		int numE = n->getNumEdges(true);
		for (int i = 0; i < numE; i++){
			int v = n->getEdgeAt(i, true);
			if (color[v] > 0) continue;
			color[v] = color[u] + 1;
			queue.push_back(v);
		}
		if (queue.size() == 0) break;
		u = queue[0];
		queue.erase(queue.begin());
	}
	int dia = 1;
	for (int i = 0; i < numNodes; i++)
		if (dia < (color[i]-1))
			dia = color[i]-1;
	return dia;
}

int Graph::buildStops(int src, bool direction, int label_id){

	
	Node *stops = srcStops;
	if (!direction) stops = dstStops;
	

	int stopIndex = 0;
	stops[stopIndex] = *nodes[src];

	for (int i = 0; i < numWalks; i++){

		stack<int> cur_stack;
		int currNode = src;
		cur_stack.push(currNode);
		vector<int> visited_nodes;
		visited_nodes.push_back(currNode);

		int e = src;
		// for (int j = 0; j < walkLength && !cur_stack.empty(); j++){

		// 	currNode = cur_stack.top();
		// 	Node *n = nodes[currNode];
			
		// 	e = n->sampleEdge(direction,visited_nodes);

		// 	if (e == -1){
		// 		cur_stack.pop();
		// 	}
		// 	else
		// 	{	
		// 		visited_nodes.push_back(e);
		// 		cur_stack.push(e);
		// 	}
		// }

		for (int j = 0; j < walkLength && !cur_stack.empty(); j++){
			Node *n = nodes[e];			
			e = n->sampleEdge(direction,visited_nodes);

			if (e == -1){
				break;
			}

		stopIndex += 1;
		if (stopIndex <= numStops){
			stops[stopIndex] = *nodes[e];				
			}
		}
		

		
	}
	return stopIndex;
}


int Graph::isReachable(int src, int dst, int label_id, int c, bool fractional,int control){ //control=1 for BFS, 0 for RW
	if(src==dst) return 1;

	if(control){
		bool isReached[numNodes];
		std::vector<int> queue;
		for (int i = 0; i < numNodes; i++) isReached[i] = false;
		int u = src;
		isReached[u] = true;
		queue.push_back(u);
		while (queue.size() > 0){
			u = queue[0];
			queue.erase(queue.begin());
			//for each edge from u
			Node *n = nodes[u];

			for (int i = 0; i < n->getNumEdges(true); i++){
				int v = n->getEdgeAt(i,true);
				if (isReached[v] == false){
					isReached[v] = true;
					if(v==dst)
						return 1;
					queue.push_back(v);
				}
			}
	}

	}


	else
	{	

		int numSrcStops = buildStops(src, true, label_id);
		int numDstStops = buildStops(dst, false, label_id);
		int answer = 0;

		bool flag = false;
		for (int i = 0; i < numSrcStops; i++){
			if (srcStops[i].nodeId == -1) continue;
			for (int j = 0; j < numDstStops; j++){
				if (dstStops[j].nodeId == -1) continue;
				 if(dstStops[j].nodeId==srcStops[i].nodeId)
				 	return 1;
			}
		}
	}
	return 0;
}


