import glob

edge_filenames = glob.glob('*.edges');
assn_filenames = glob.glob('*.feat');
edge_filenames = glob.glob('*.featnames');

nodes = []
out_nodes = [[]] * 10000
in_nodes  = [[]] * 10000
label_names = {}
all_features = []


for ind in range(len(edge_filenames)):
	edge_file = open(edge_filenames[ind],'r')
	assn_file = open(assn_filenames[ind],'r')
	feat_file = open(feat_filenames[ind],'r')

	feature_names = [""] * 10000

	for line in feat_file.readlines():
		i = line.split()[0]
		feature_names[int(i)] = line[len(i)+1:].strip()
		all_features.append(line[len(i)+1:].strip())

	for line in assn_file.readlines():
		ws = line.split()
		node = int(ws[0])
		label_names[node] = []
		nodes.append(int(ws[0]))
		for i in range(1,len(ws)):
			if(ws[i]=="1"):
				label_names[int(ws[0])].append(feature_names[i-1])

	for line in edge_file.readlines():
		ws = line.split()
		out_nodes[int(ws[0])].append(int(ws[1]))
		in_nodes[int(ws[0])].append(int(ws[1]))

		out_nodes[int(ws[1])].append(int(ws[0]))
		in_nodes[int(ws[1])].append(int(ws[0]))

all_features = list(set(all_features))


feat_ofile = open("facebook_combined.featnames","w")
assn_ofile = open("facebook_combined.feat","w")

nodes.sort()
#print(nodes)

for i in range(len(all_features)):
	print >>feat_ofile, str(i)+" "+all_features[i]+"\n"

for node in nodes:
	s = str(node) + " "
	for i in range(len(all_features)):
		if(all_features[i] in label_names[node]):
			s += "1"
		else:
			s += "0"
	s += "\n"
	print >>assn_ofile, s

