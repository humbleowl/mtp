#include <vector>
#include <queue>
#include <string>
#include <iostream>
#include <stdlib.h>
#include <cstdlib>
#include <ctime>
#include <fstream>
#include <string>
#include <string.h>
#include <math.h>
#include <queue>
#include <stack>

#include <time.h>
#include <sys/time.h>

#ifdef __MACH__
#include <mach/clock.h>
#include <mach/mach.h>
#endif

using namespace std;

class Node{

public:
	Node(int isNodeId, std::string isNodeLabelling="");
	void addEdge(int destId, bool direction);

	int getNumEdges(bool direction);
	bool has_label(int label_id);
	int getEdgeAt(int i, bool direction);
	int sampleEdge(bool direction, vector<int> visited);
    void set_label(std::string s);

    int nodeId;
private:
	std::string labelling;
	std::vector<int> fwdEdges;
	std::vector<int> bwdEdges;
	int numFwdEdges;
	int numBwdEdges;
};

Node::Node(int isNodeId, std::string isNodeLabelling){
	nodeId = isNodeId;
	labelling = isNodeLabelling;
	numFwdEdges = 0;
	numBwdEdges = 0;
}

void Node::set_label(std::string s){
	labelling = s;
}
void Node::addEdge(int destId, bool direction){
	if (direction){
		fwdEdges.push_back(destId);
		numFwdEdges += 1;
	}
	else{
		bwdEdges.push_back(destId);
		numBwdEdges += 1;
	}
}

int Node::getNumEdges(bool direction){
	if (direction) return numFwdEdges;
	return numBwdEdges;
}

int Node::getEdgeAt(int i, bool direction){
	if (direction) return (i<numFwdEdges)?fwdEdges[i]:-1;
	return (i<numBwdEdges)?bwdEdges[i]:-1;
}

int Node::sampleEdge(bool direction, vector<int> visited){
	int n = getNumEdges(direction);
	vector<int> cand;

	if(direction){
		int k = 0;
		for(int i=0;i<numFwdEdges;i++)
		{	
			int flag = 0;
			for(int j=0;j<visited.size();j++)
				if(fwdEdges[i]==visited[j])
					flag = 1;
			if(!flag)
			{
			  cand.push_back(fwdEdges[i]);
			   k++;
			}
		}
	    if(k>0) return cand[rand()%k]; else return -1;
	}
	else{
		int k = 0;
		for(int i=0;i<numBwdEdges;i++)
		{	
			int flag = 0;
			for(int j=0;j<visited.size();j++)
				if(bwdEdges[i]==visited[j])
					flag = 1;
			if(!flag)
			{
			  cand.push_back(bwdEdges[i]);
			   k++;
			}
		}				
	    if(k>0) return cand[rand()%k]; else return -1;
	}
	return -1;
}
